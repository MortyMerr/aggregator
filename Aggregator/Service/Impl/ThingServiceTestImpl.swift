//
//  ThingServiceTestImpl.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import ObjectMapper

class ThingServiceTestImpl: ThingService {
  lazy var things = getThings()
  
  func getAll() -> [Thing] {
    return things
  }
  
  func getAll(brand: Int64) -> [Thing] {
    return things.filter{ $0.brandId == brand }
  }
  
  func getAll(query: String!, brand: [Int64], categories: [String], priceMin: Double, priceMax: Double, gender: String) -> [Thing] {
    return things
      .filter{ $0.gender == gender.lowercased() }
      .filter{ $0.price > priceMin && $0.price < priceMax }
      .filter{ categories.contains($0.category) }
      .filter{ brand.contains($0.brandId) }
      .filter{ query.isEmpty ||
        $0.name.lowercased().contains(query.lowercased())}
  }
  
  private func getThings() -> [Thing] {
    return Mapper<Thing>().mapArray(JSONString: Utils.getJSON(name: "thing"))!
  }
}
