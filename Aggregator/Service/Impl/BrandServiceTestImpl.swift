//
//  BrandServiceTestImpl.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import ObjectMapper

class BrandServiceTestImpl: BrandService {
  lazy var brands = getBrands()
  func getAll() -> [Brand] {
    return brands
  }
  
  func getBy(id: Int64) -> Brand {
    return brands[Int(id)]
  }
  
  private func getBrands() -> [Brand] {
    return Mapper<Brand>().mapArray(JSONString: Utils.getJSON(name: "brand"))!
  }
}
