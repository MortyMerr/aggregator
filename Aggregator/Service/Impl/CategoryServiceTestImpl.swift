//
//  CategoryServiceTestImpl.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

class CategoryServiceTestImpl {
  lazy var categories = getCategories()
  func getAll() -> [String] {
    return categories
  }
  
  private func getCategories() -> [String] {
    return ["Футболки", "Свитшоты"]
  }
}
