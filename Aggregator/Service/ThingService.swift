//
//  ThingService.swift
//  Aggregator
//
//  Created by Антон Назаров on 27.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

protocol ThingService {
  func getAll() -> [Thing]
  
  func getAll(brand: Int64) -> [Thing]
}
