//
//  CategoryService.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

protocol CategoryService {
  func getAll() -> [String]
}
