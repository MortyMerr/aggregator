//
//  BrandService.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

protocol BrandService {
  func getAll() -> [Brand]
  func getBy(id: Int64) -> Brand
}
