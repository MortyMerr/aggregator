//
//  OrderDetails.swift
//  Aggregator
//
//  Created by Антон Назаров on 25/05/2018.
//  Copyright © 2018 MortyMerr. All rights reserved.
//

import UIKit
import Eureka

class OrderDetailsViewController: FormViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++

            Section("Личные данные")

            <<< NameRow() {
                $0.title = "Имя"
                $0.value = "Иван"
            }

            <<< NameRow() {
                $0.title = "Фамилия"
                $0.value = "Иванов"
            }
            <<< TextRow() {
                $0.title = "Адрес"
                $0.value = "Сан"
            }
            <<< PhoneRow() {
                $0.title = "Телефон"
                $0.value = "89119324309"
            }

            <<< EmailRow() {
                $0.title = "Email"
                $0.value = "ivanov@mail.ru"
            }

            <<< TextRow() {
                $0.title = "Паспорт"
                $0.value = ""
        }

    }
}
