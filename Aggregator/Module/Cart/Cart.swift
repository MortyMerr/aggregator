//
//  Cart.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

class Cart {
  static var cart = [Thing]()
  static var total: Double {
    get{
      return cart.reduce(0, { (r, t) -> Double in
        r + t.price
      })
    }
  }
}
