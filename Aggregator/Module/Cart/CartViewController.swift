//
//  CartViewController.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    static let CELL_INDENTIFIER = "cartCell"

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var total: UILabel!

    @IBAction func clearAll(_ sender: Any) {
        let alertView = UIAlertController(title: "Очистить все?", message: "Действительно хотите очистить корзину?", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Нет", style: .default, handler: nil))
        alertView.addAction(UIAlertAction(title: "Очистить", style: .destructive, handler: { (alertAction) -> Void in
            Cart.cart.removeAll()
            self.reloadAll()
        }))
        present(alertView, animated: true, completion: nil)
    }

    @IBAction func order(_ sender: Any) {
//        let alertView = UIAlertController(title: "Отправк заказа", message: "Вы правда хотите оформить заказ?", preferredStyle: .alert)
//        alertView.addAction(UIAlertAction(title: "Да", style: .default, handler: { _ in
//            let alertView = UIAlertController(title: "Успех", message: "Заказ успешно отправлен", preferredStyle: .alert)
//            alertView.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
//            self.present(alertView, animated: true, completion: nil)
//            Cart.cart.removeAll()
//            self.reloadAll()
//        }))
//        alertView.addAction(UIAlertAction(title: "Отмена", style: .destructive, handler: nil))
//        present(alertView, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        reloadAll()
    }

    func reloadAll() {
        tableView.reloadData()
        total.text! = String(Cart.total)
    }
}

extension CartViewController: UITableViewDelegate {

}

extension CartViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Cart.cart.count
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            Cart.cart.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath as IndexPath], with: .right)
            tableView.endUpdates()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CartViewController.CELL_INDENTIFIER, for: indexPath)
        cell.textLabel!.text = Cart.cart[indexPath.row].name
        cell.detailTextLabel!.text = "₽" + String(Cart.cart[indexPath.row].price)
        return cell
    }
}
