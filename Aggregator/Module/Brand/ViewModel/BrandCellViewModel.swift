//
//  BrandCellViewModel.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import UIKit
import Kingfisher

class BrandCellViewModel {
  let imageUrl: URL
  let id: Int64
  let name: String
  var choosen: Bool
  
  init(imageUrl: String, id: Int64, name: String) {
    self.imageUrl = URL(string: imageUrl)!
    self.id = id
    self.name = name
    self.choosen = false
  }
  
  static func configureCell(_ cell: UICollectionViewCell, for brand: BrandCellViewModel) {
    let brandCell = cell as! BrandCollectionViewCell
    brandCell.brandImageView.kf.setImage(with: brand.imageUrl)
  }
}
