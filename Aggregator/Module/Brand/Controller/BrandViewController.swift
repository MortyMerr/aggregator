//
//  BrandViewController.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import UIKit

class BrandViewController: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  static let CELL_INDETIFIER = "brandCell"
  private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
  private let itemsPerRow: CGFloat = 2
  
  //fixme
  let brands = BrandServiceTestImpl().getAll().map { BrandCellViewModel(imageUrl: $0.imageUrl, id: $0.id, name: $0.name) }
  
  override func viewDidLoad() {
    collectionView.delegate = self
  }
}

extension BrandViewController: UICollectionViewDelegate {
}

extension BrandViewController: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return brands.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandViewController.CELL_INDETIFIER, for: indexPath)
    BrandCellViewModel.configureCell(cell, for: brands[indexPath.row])
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let controller = storyboard!.instantiateViewController(withIdentifier: ThingViewController.INDENTIFIER) as! ThingViewController
    controller.thingViewModel = ThingViewModel(thingService: ThingServiceTestImpl())
    controller.thingViewModel.enteredBrand = brands[indexPath.row].id
    navigationController?.pushViewController(controller, animated: true)
  }
}

extension BrandViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize {
    let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
    let availableWidth = view.frame.width - paddingSpace
    let widthPerItem = availableWidth / itemsPerRow
    
    return CGSize(width: widthPerItem, height: widthPerItem)
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      insetForSectionAt section: Int) -> UIEdgeInsets {
    return sectionInsets
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return sectionInsets.left
  }
}
