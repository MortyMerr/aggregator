//
//  ThingViewModel.swift
//  Aggregator
//
//  Created by Антон Назаров on 27.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

class ThingViewModel {
  let thingService: ThingServiceTestImpl
  var enteredBrand: Int64! {
    didSet {
      things = thingService.getAll(brand: enteredBrand)
    }
  }
  
  var things: [Thing]!
  
  init(thingService: ThingServiceTestImpl) {
    self.thingService = thingService
  }
}
