//
//  ThingCollectionViewCell.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import UIKit

class ThingCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var img: UIImageView!
  @IBOutlet weak var descr: UITextView!
  @IBOutlet weak var price: UIButton!
}
