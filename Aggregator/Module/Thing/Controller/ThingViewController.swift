//
//  ThingViewController.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import UIKit

class ThingViewController: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  
  static let INDENTIFIER = "thingViewController"
  static let CELL_INDETIFIER = "thingCell"
  private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
  private let itemsPerRow: CGFloat = 2
  var thingViewModel: ThingViewModel!
  override func viewDidLoad() {
    collectionView.delegate = self
  }
}

extension ThingViewController: UICollectionViewDelegate {
}

extension ThingViewController: UICollectionViewDataSource {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return thingViewModel.things.count
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let concreteThing = storyboard?.instantiateViewController(withIdentifier: ConcreteThingViewController.IDENTIFIER) as! ConcreteThingViewController
    concreteThing.thing = thingViewModel.things[indexPath.row]
    navigationController?.pushViewController(concreteThing, animated: true)
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ThingViewController.CELL_INDETIFIER, for: indexPath) as! ThingCollectionViewCell
    //fixme
    let thing = thingViewModel.things[indexPath.row]
    cell.descr.text = "•" + thing.descr.joined(separator: "\n•")
    cell.img.kf.setImage(with: thing.imageUrl)
    cell.img.layer.cornerRadius = 15
    cell.img.layer.masksToBounds = true
    cell.name.text! = thing.name
    
    cell.price.setTitle(String(thing.price), for: .normal)
    return cell
  }
}

