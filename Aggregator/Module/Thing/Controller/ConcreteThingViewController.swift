//
//  ConcreteThingViewController.swift
//  Aggregator
//
//  Created by Антон Назаров on 27.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import UIKit

class ConcreteThingViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    private let size = ["M", "L", "XL"]
    private let colores = ["Black", "White", "Orange", "Yellow"]
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == 1 ? size.count : colores.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return component == 1 ? size[row] : colores[row]
    }

  static let IDENTIFIER = "ConcreteThingViewController"
  var thing: Thing!
  var index: Int = 0
  //fixme
  var currentColor: String! = "Black"
  var currentSize: String! = "X"
  
    @IBOutlet var picker: UIPickerView!
    @IBAction func addToCart(_ sender: Any) {
    let alert = UIAlertController(title: thing.name, message: "Добавить в корзину цвет \(currentColor!), размер \(currentSize!)?", preferredStyle: UIAlertControllerStyle.alert)
    alert.addAction(UIAlertAction(title: "Да", style: UIAlertActionStyle.default, handler: { _ in Cart.cart.append(self.thing)}))
    alert.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.destructive, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
  
  @IBOutlet weak var image: UIImageView!
  @IBOutlet weak var colorLayour: UIStackView!
  
  @objc func buttonColorClicked(sender: UIButton!) {
    currentColor = thing.colors[sender.tag]
  }
  
  @objc func buttonSizeClicked(sender: UIButton!) {
    currentSize = sender.titleLabel?.text
  }



  @IBOutlet weak var descr: UITextView!
  @IBOutlet weak var sizes: UIStackView!
  override func viewDidLoad() {
    super.viewDidLoad()
    picker.dataSource = self
    picker.delegate = self
    image.kf.setImage(with: thing.imageUrl)
    image.layer.cornerRadius = 10
    descr.text = thing.info
    colorLayour.spacing = 5
    colorLayour.distribution = .fillEqually
    
    for i in 0..<thing.colors.count {
      let view = UIButton()
      
      view.addTarget(self, action: #selector(buttonColorClicked), for: .touchUpInside)
      view.layer.borderColor = UIColor.gray.cgColor
      view.layer.borderWidth = 1
      view.layer.cornerRadius = 10
      view.backgroundColor = Mappers.colorFrom(name: thing.colors[i])
      view.tag = i
      colorLayour.addArrangedSubview(view)
    }
    colorLayour.reloadInputViews()
    
    sizes.spacing = 5
    thing.sizes.map{
      let view = UIButton()
      view.addTarget(self, action: #selector(buttonSizeClicked), for: .touchUpInside)
      if !$0.available {
        view.backgroundColor = .lightGray
      }
      view.setTitleColor(.black, for: .normal)
      view.layer.cornerRadius = 10
      view.setTitle($0.name, for: .normal)
      return view
      }
      .forEach(sizes.addArrangedSubview)
    
    let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
    swipeRight.direction = UISwipeGestureRecognizerDirection.right
    self.view.addGestureRecognizer(swipeRight)
    
    let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
    swipeLeft.direction = UISwipeGestureRecognizerDirection.left
    self.view.addGestureRecognizer(swipeLeft)
  }
  
  @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
    if let swipeGesture = gesture as? UISwipeGestureRecognizer {
      switch swipeGesture.direction {
      case UISwipeGestureRecognizerDirection.right:
        if index + 1 < thing.url.count {
          index += 1
        } else {
          index = 0
        }
      case UISwipeGestureRecognizerDirection.left:
        if index - 1 >= 0 {
          index -= 1
        } else {
          index = thing.url.count - 1
        }
      default:
        break
      }
      replaceImage()
    }
  }
  
  func replaceImage() {
    image.kf.setImage(with: URL(string: thing.url[index])!)
  }
}
