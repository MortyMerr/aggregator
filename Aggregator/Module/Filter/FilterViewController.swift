//
//  FilterViewController.swift
//  Aggregator
//
//  Created by Антон Назаров on 27.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import RxSwift
import RxCocoa
import TTRangeSlider

class FilterViewController: UIViewController {
  private let disposeBag = DisposeBag()
  @IBOutlet weak var searchBar: UISearchBar!
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var scope: UISegmentedControl!
  
  @IBOutlet weak var category: UITableView!
  @IBOutlet weak var slider: TTRangeSlider!
  let thingCategories = ["Футболки", "Свитшоты"]
  
  var brands = BrandServiceTestImpl().getAll().map { BrandCellViewModel(imageUrl: $0.imageUrl, id: $0.id, name: $0.name)}
  
  @IBAction func buttonPressed(_ sender: Any) {
    let viewController = storyboard?.instantiateViewController(withIdentifier: "thingViewController") as! ThingViewController
    viewController.thingViewModel = ThingViewModel(thingService: ThingServiceTestImpl())
    var tmpBrands = [Int64]()
    var tmpCategories = [String]()
    for i in 0..<4 {
      let tmpCell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as! Cell
      if !tmpCell.button.isHidden {
        tmpBrands.append(Int64(i + 1))
      }
    }
    
    for i in 0..<thingCategories.count {
      let tmpCell = category.cellForRow(at: IndexPath(row: i, section: 0)) as! Cell
      if !tmpCell.button.isHidden {
        tmpCategories.append(thingCategories[i])
      }
    }
    viewController.thingViewModel.things = viewController.thingViewModel.thingService.getAll(query: searchBar.text, brand: tmpBrands, categories: tmpCategories, priceMin: Double(slider.selectedMinimum), priceMax: Double(slider.selectedMaximum), gender: scope.titleForSegment(at: scope.selectedSegmentIndex)!)
    navigationController?.pushViewController(viewController, animated: true)
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    slider.numberFormatterOverride = NumberFormatter()
    slider.numberFormatterOverride.positivePrefix = "₽"
    slider.minValue = 100
    slider.maxValue = 15000
    slider.selectedMinimum = 2000
    slider.selectedMaximum = 3000
    
    
    let items = Observable.just(brands)
    
    let categories = Observable.just(thingCategories)
    
    
    categories
      .bind(to: category.rx.items(cellIdentifier: "CELL", cellType: Cell.self)) { (row, element, cell) in
        cell.textLabel?.text = element
        cell.selectionStyle = .none
      }
      .disposed(by: disposeBag)
    
    
    category.rx
      .itemSelected
      .subscribe(onNext: { indexPath in
        let cell = self.category.cellForRow(at: indexPath) as! Cell
        cell.button.isHidden = !cell.button.isHidden
      })
      .disposed(by: disposeBag)
    
    items
      .bind(to: tableView.rx.items(cellIdentifier: "CELL", cellType: Cell.self)) { (row, element, cell) in
        cell.textLabel?.text = element.name
        cell.imageView?.kf.setImage(with: element.imageUrl)
        cell.selectionStyle = .none
      }
      .disposed(by: disposeBag)
    
    
    tableView.rx
      .itemSelected
      .subscribe(onNext: { indexPath in
        let cell = self.tableView.cellForRow(at: indexPath) as! Cell
        cell.button.isHidden = !cell.button.isHidden
      })
      .disposed(by: disposeBag)
  }
}

extension FilterViewController: TTRangeSliderDelegate {
  func rangeSlider(_ sender: TTRangeSlider!, didChangeSelectedMinimumValue selectedMinimum: Float, andMaximumValue selectedMaximum: Float) {
    
  }
}
