//
//  Mappers.swift
//  Aggregator
//
//  Created by Антон Назаров on 27.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import UIKit

class Mappers {
  static func colorFrom(name: String) -> UIColor{
    switch name {
    case "White":
      return .white
    case "Black":
      return .black
    case "Red":
      return .red
    case "Blue":
      return .blue
    default:
      return .clear
    }
  }
}
