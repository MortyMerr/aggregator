//
//  Brand.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import ObjectMapper

class Brand {
  var id: Int64!
  var imageUrl: String!
  var name: String!
  
  required convenience init?(map: Map) {
    self.init()
    mapping(map: map)
  }
}

extension Brand: Mappable {
  func mapping(map: Map) {
    id <- map["id"]
    imageUrl <- map["imageUrl"]
    name <- map["name"]
  }
}
