//
//  Size.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import ObjectMapper

class Size {
  var name: String!
  var available: Bool!
  
  required convenience init?(map: Map) {
    self.init()
    mapping(map: map)
  }
}

extension Size: Mappable {
  func mapping(map: Map) {
    name <- map["name"]
    available <- map["available"]
  }
}
