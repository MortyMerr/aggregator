//
//  Thing.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import ObjectMapper

class Thing {
  var brandId: Int64!
  var name: String!
  var category: String!
  var gender: String!
  var sizes: [Size]!
  var descr: [String]!
  var info: String!
  var colors: [String]!
  var url: [String]!
  var price: Double!
  var imageUrl: URL! {
    return URL(string: url[0])
  }
  required convenience init?(map: Map) {
    self.init()
    mapping(map: map)
  }
}

extension Thing: Mappable {
  func mapping(map: Map) {
    brandId <- map["brandId"]
    url <- map["imageUrl"]
    descr <- map["descr"]
    info <- map["info"]
    sizes <- map["sizes"]
    name <- map["name"]
    price <- map["price"]
    colors <- map["colors"]
    gender <- map["gender"]
    category <- map["category"]
  }
}
