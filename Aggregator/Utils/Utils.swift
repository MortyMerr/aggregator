//
//  Util.swift
//  Aggregator
//
//  Created by Антон Назаров on 22.10.2017.
//  Copyright © 2017 MortyMerr. All rights reserved.
//

import Foundation

class Utils {
  public static func getJSON(name: String) -> String {
    guard let path = Bundle.main.path(forResource: name, ofType: "json") else {
      fatalError("Can't read \(name)")
    }
    return try! String(contentsOfFile: path, encoding: .utf8)
  }
}
